<h1 align="center">pixerama test</h1>


## O que é

Implantação de um Sistema simplificado de CRM

## Necessario para rodar o Projeto
- mysql 8
- php 8.1^
- laravel 10^



## Começando

```
# clone o projeto 
git@gitlab.com:DeykyPinheiro/pixerama-test.git

# entre na pasta
cd pixerama-test

#rode as migrations
php artisan migrate

#rode o bootstrap
npm install && npm run dev

#rode o projeto
php artisan serve
```




## Tecnologias utilizadas
- Laravel
- php 8.2
- Mysql 8.0

## Rodar testes
```
#migrations de test
php artisan migrate --env=testing

#rodar teste
php artisan test  --env=testing
```


## Autor

Deyky Pinheiro

[![LinkdIn Badge](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=whit)](https://www.linkedin.com/in/deyky-pinheiro-bbb735125/)
